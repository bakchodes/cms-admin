import { Injectable } from '@angular/core';
import { Subject, fromEvent, Observable } from 'rxjs';

@Injectable()
export class ConnectionService {

  public isOnlineValue = true;
  private isOnline$ = new Subject<boolean>();
  constructor() {
    this.subscribe();
  }

  isOnline() {
    return this.isOnline$.asObservable();
  }

  private subscribe() {
    fromEvent(window, 'offline').subscribe(() => {
      this.isOnlineValue = false;
      this.isOnline$.next(this.isOnlineValue);
    });
    fromEvent(window, 'online').subscribe(() => {
      this.isOnlineValue = true;
      this.isOnline$.next(this.isOnlineValue);
    });
  }

}
