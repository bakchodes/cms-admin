import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router, CanActivate, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(): Observable<boolean> | boolean {
    return this.authService.isLoggedIn().pipe(map((loginStatus) => {
      if (!loginStatus) {
        this.router.navigate(['/login']);
        return false;
      } else {
        return true;
      }
    }));
  }

}
