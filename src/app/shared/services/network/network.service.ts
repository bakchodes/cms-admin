import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Api } from '../../constants/api';


@Injectable()
export class NetworkService {

  constructor(
    private http: HttpClient,
  ) {
  }

  public get<T>(url: string, baseUrl?: string): Observable<T> {
    return this.http.get<T>(baseUrl ? baseUrl + url : Api.BASE_URL + url);
  }

  public post<T>(url: string, baseUrl?: string, data?: any): Observable<T> {
    return this.http.post<T>(baseUrl ? baseUrl + url : Api.BASE_URL + url, data);
  }

  public put<T>(url: string, baseUrl?: string, data?: any): Observable<T> {
    return this.http.put<T>(baseUrl ? baseUrl + url : Api.BASE_URL + url, data);
  }

  public delete<T>(url: string, baseUrl?: string): Observable<T> {
    return this.http.delete<T>(baseUrl ? baseUrl + url : Api.BASE_URL + url);
  }

}
