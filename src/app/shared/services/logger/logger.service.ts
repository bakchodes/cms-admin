import { Injectable } from'@angular/core';

@Injectable()
export class LoggerService {

  constructor() { }

  messages: string[] = [];

  public log(message: string) {
    console.log(message);
  }

  public error(message?: any, ...optionalParams: any[]): void {
    console.log(message, optionalParams);
  }

  public add(message: string) {
    this.messages.push(message);
  }

  public clear() {
    this.messages = [];
  }
}
