import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './auth/auth.service';
import { AuthGuardService } from './auth-guard/auth-guard.service';
import { LoggerService } from './logger/logger.service';
import { NetworkService } from './network/network.service';
import { StorageService } from './storage/storage.service';
import { ToastService } from './toast/toast.service';
import { NonAuthGuardService } from './non-auth-guard/non-auth-guard.service';
import { LoaderService } from './loader/loader.service';
import { ConnectionService } from './connection/connection.service';
import { MasterDataService } from './master-data/master-data.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ServicesModule {

  constructor(@Optional() @SkipSelf() parentModule: ServicesModule) {
    if (parentModule) {
      throw new Error(
        'ServicesModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders<ServicesModule> {
    return {
      ngModule: ServicesModule,
      providers: [
        AuthService,
        AuthGuardService,
        NonAuthGuardService,
        LoggerService,
        NetworkService,
        StorageService,
        ToastService,
        LoaderService,
        ConnectionService,
        MasterDataService
      ]
    };
  }
}
