import { Injectable } from '@angular/core';
import { NetworkService } from '../network/network.service';
import { MasterData, MasterDataMap } from '../../dtos/master-data';
import { Api } from 'src/app/shared/constants/api';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';
import { AppState } from '../../modules/store';
import { Store } from '@ngrx/store';
import { loadBoards, loadTuitionTypes, loadInteractionTypes, loadExams, loadCourseTypes } from '../../modules/store/master/master.action';

@Injectable()
export class MasterDataService {

  private countries: MasterDataMap;
  private statesByCountryId: { [key: string]: MasterDataMap };
  private citiesByStateId: { [key: string]: MasterDataMap };
  private specializations: MasterDataMap;
  private courses: MasterDataMap;
  private educations: MasterDataMap;
  private universities: MasterDataMap;
  private courseTypes: MasterDataMap;
  private standards: MasterDataMap;
  private subjects: MasterDataMap;
  private boards: MasterDataMap;
  private languages: MasterDataMap;
  private disabilities: MasterDataMap;

  private yearsAcademics: MasterDataMap;
  private yearsExperiences: MasterDataMap;

  constructor(
    private store: Store<AppState>,
    private networkService: NetworkService
  ) {
    this.statesByCountryId = {};
    this.citiesByStateId = {};
  }

  public async getCountries() {
    if (!this.countries) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_COUNTRY_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.countries = data;
              resolve();
          });
      });
    }
    return this.countries;
  }

  public async getStatesByCountryId(countryid) {
    if (this.statesByCountryId[countryid] === undefined) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_STATE_GETALL_BY_COUNTRY(countryid), null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
            this.statesByCountryId[countryid] = data;
            resolve();
          });
      });
    }
    return this.statesByCountryId[countryid];
  }

  public async getCitiesByStateId(stateid) {
    if (this.citiesByStateId[stateid] === undefined) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_CITY_GETALL_BY_STATE(stateid), null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
            this.citiesByStateId[stateid] = data;
            resolve();
          });
      });
    }
    return this.citiesByStateId[stateid];
  }
  public async getSpecializations() {
    if (!this.specializations) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_SPECIALIZATION_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.specializations = data;
              resolve();
          });
      });
    }
    return this.specializations;
  }
  public async getCourses() {
    if (!this.courses) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_COURSE_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.courses = data;
              resolve();
          });
      });
    }
    return this.courses;
  }
  public async getEducations() {
    if (!this.educations) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_EDUCATION_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.educations = data;
              resolve();
          });
      });
    }
    return this.educations;
  }

  public async getUniversities() {
    if (!this.universities) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_UNIVERSITY_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.universities = data;
              resolve();
          });
      });
    }
    return this.universities;
  }

  public async getCourseType() {
    if (!this.courseTypes) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_COURSETYPE_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.courseTypes = data;
              resolve();
          });
      });
    }
    return this.courseTypes;
  }

  public async getStandards() {
    if (!this.standards) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_STANDARD_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.standards = data;
              resolve();
          });
      });
    }
    return this.standards;
  }

  public async getSubjects() {
    if (!this.subjects) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_SUBJECT_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.subjects = data;
              resolve();
          });
      });
    }
    return this.subjects;
  }

  public async getBoards() {
    if (!this.boards) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_BOARD_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.boards = data;
              resolve();
          });
      });
    }
    return this.boards;
  }

  public async getLanguages() {
    if (!this.languages) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_LANGUAGE_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.languages = data;
              resolve();
          });
      });
    }
    return this.languages;
  }

  public async getDisabilities() {
    if (!this.disabilities) {
      await new Promise(resolve => {
        this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_DISABILITY_GETALL, null)
          .pipe(
            map(res => this.arrayToMap(res))
          )
          .subscribe((data) => {
              this.disabilities = data;
              resolve();
          });
      });
    }
    return this.disabilities;
  }

  public getYearsAcademics() {
    if (!this.yearsAcademics) {
      let years: MasterData[] = [];
      for (let i=1960; i<=2020; i++) years.push({id: i, name: i.toString()});
      this.yearsAcademics = this.arrayToMap(years);
    }
    return this.yearsAcademics;
  }

  public getYearsExperience() {
    if (!this.yearsExperiences) {
      let years = [{id: 0, name: 'No experience'}, {id: 1, name: '1 Year'}];
      for (let i = 2; i< 51; i++) years.push({id: i, name: i + ' Years'});
      this.yearsExperiences = this.arrayToMap(years);
    }
    return this.yearsExperiences;
  }

  public getTuitions() {
    return [
      {name: 'Offline Tuition', id: 1},
      {name: 'Online Tuition', id: 2},
    ];
  }

  public getInteractions() {
    return [
      {name: 'One 2 One', id: 1, price: {type: 'Month', low: 4000, high: 6000}},
      {name: 'Batch', id: 2, price: {type: 'Month', low: 2000, high: 3500}},
    ];
  }

  private arrayToMap(data: MasterData[]): MasterDataMap {
    let mapped_data: MasterDataMap = {};
    data.forEach(element => {
      mapped_data[element.id] = element;
    });
    return mapped_data;
  }

  // Added by Deepanshu
  public getBoards$() {
    return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_BOARD_GETALL);
  }

  public getBT$() {
    return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_BT_GETALL);
  }

  public getDifficultyLevel$() {
    return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_DL_GETALL);
  }

  public getQuestionType$() {
    return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_QUESTION_TYPE_GETALL);
  }

  public getQuestionStatus$() {
    return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_QUESTION_STATUS_GETALL);
  }

  public getStandards$() {
    return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_STANDARD_GETALL);
  }

  public getCitiesByState$(stateId: number) {
    return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_CITY_GETALL_BY_STATE(stateId));
  }

  public getCitiesByIds$(ids:number[]){
    return this.networkService.post<MasterData[]>("",null,ids);
  }

  public getStatesByIds$(ids:number[]){
    return this.networkService.post<MasterData[]>("",null,ids);
  }

  public getSubjects$() {
    return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_SUBJECT_GETALL);
  }

  public getExams$() {
    const exams: MasterData[] = [
      {name: 'NTSE', id: 1},
      {name: 'NSO', id: 2},
      {name: 'KYPS', id: 3},
    ];
    return of(exams);
    // return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_BOARD_GETALL);
  }

  public getTuitionTypes$() {
    // const tuitions: MasterData[] = [
    //   {name: 'Offline Tuition', id: 1},
    //   {name: 'Online Tuition', id: 2},
    // ];
    // return of(tuitions);
    return this.networkService.get<MasterData[]>("");
  }

  public getInteractionTypes$() {
    return this.networkService.get<MasterData[]>("");
  }

  public getCourseTypes$() {
    return this.networkService.get<MasterData[]>(Api.TUTOR_FORM_OPTIONS_COURSETYPE_GETALL);
  }

}
