import { Injectable, TemplateRef } from '@angular/core';

@Injectable()
export class ToastService {

  constructor() { }

  toasts: any[] = [];

  private show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts = []; // to avoid multiple toasts at one time
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  showStandard(message: string) {
    this.show(message, {
      delay: 2000,
      autohide: true,
    });
  }

  showSuccess(message: string) {
    this.show(message, {
      classname: 'bg-success ',
      delay: 2000,
      autohide: true,
      // headertext: 'Toast Header'
    });
  }

  showError(message: string) {
    this.show(message, {
      classname: 'bg-danger text-light',
      delay: 2000,
      autohide: true,
      // headertext: 'Error!!!'
    });
  }

  showCustomToast(customTpl) {
    this.show(customTpl, {
      classname: 'bg-info text-light',
      delay: 2000,
      autohide: true
    });
  }

}
