import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { map } from 'rxjs/operators';

@Injectable()
export class NonAuthGuardService implements CanActivate {

  constructor(
    private authService: AuthService, 
    private router: Router
  ) { }

  canActivate(): Observable<boolean> | boolean {
    return this.authService.isLoggedIn().pipe(map((loginStatus) => {
      if (loginStatus) {
        this.router.navigate(['/']);
        return false;

      } else {
        return true;
      }
    }));
  }
}
