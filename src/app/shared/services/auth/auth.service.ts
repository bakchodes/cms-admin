import { Injectable } from '@angular/core';
import { StorageService } from '../storage/storage.service';
import { Observable, BehaviorSubject } from 'rxjs';
@Injectable()
export class AuthService {

  private isLoggedIn$: BehaviorSubject<boolean>;

  constructor(private storageService: StorageService) {
    this.isLoggedIn$ = new BehaviorSubject<boolean>(this.isToken());
  }

  getAuthorizationToken(): string {
    return this.storageService.get('accessToken');
  }

  setAuthorizationToken(value: string) {
    this.storageService.set('accessToken', value);
    this.isLoggedIn$.next(this.isToken());
  }

  setRefreshToken(value: string) {
    this.storageService.set('refreshToken', value);
  }

  removeAuthorizationToken(): void {
    this.storageService.removeItem('accessToken');
    this.isLoggedIn$.next(this.isToken());
  }

  isLoggedIn(): Observable<boolean> {
    return this.isLoggedIn$.asObservable();
  }

  private isToken(): boolean {
    const token = this.getAuthorizationToken();
    return token !== null && token !== undefined && token !== '';
  }
}
