import { NgModule } from '@angular/core';
import { CommonModule, TitleCasePipe } from '@angular/common';
import { RegisterFormComponent } from '../register-form/components/register-form/register-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SkeletonModule } from '../skeleton/skeleton.module';
import { NumberDirective } from '../../../shared/directives/numbers-only.directive';

@NgModule({
  declarations: [RegisterFormComponent, NumberDirective],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    SkeletonModule
  ],
  entryComponents: [RegisterFormComponent],
  exports: [RegisterFormComponent],
  providers: [ TitleCasePipe]

})
export class RegisterFormModule { }
