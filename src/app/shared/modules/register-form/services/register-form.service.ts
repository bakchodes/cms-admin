import { Injectable } from '@angular/core';
import { NetworkService } from 'src/app/shared/services/network/network.service';
import { Api } from 'src/app/shared/constants/api';
import { User } from 'src/app/shared/dtos/user';
import { SaveUserResponse } from 'src/app/shared/dtos/response/save-user-response';

@Injectable()
export class RegisterFormService {

  constructor(private networkService: NetworkService) { }

  saveUser(user: User){
    return this.networkService.post<SaveUserResponse>(Api.SAVE_USER, null, user);
  }
}
