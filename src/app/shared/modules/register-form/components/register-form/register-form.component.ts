import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TitleCasePipe } from '@angular/common';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ToastService } from 'src/app/shared/services/toast/toast.service';
import { User } from 'src/app/shared/dtos/user';
import { RegisterFormService } from '../../services/register-form.service.js';
import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
  providers: [RegisterFormService]
})
export class RegisterFormComponent implements OnInit, OnDestroy {

  @Input() parent: string;
  @Input() becomeTutorButton = true;
  @Input() tncLine = true;
  @Input() origin: string;
  @Output() openLogin = new EventEmitter<any>();
  signupform: FormGroup;
  submitted = false;
  returnUrl: string;
  profile: string;
  title: string;
  navigationSubscription: Subscription;

  constructor(
    private formBuilder: FormBuilder,
    private tregisterService: RegisterFormService,
    public titleCasePipe: TitleCasePipe,
    public activatedRoute: ActivatedRoute,
    private toastService: ToastService,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: object,
    @Inject(APP_ID) private appId: string) {
  }

  openVerifyOtpModal() {
    this.router.navigate([{outlets: {modal: 'modal/verify-otp'}}], {state: {username: this.signupform.value.mobile, parent: 'register', origin: this.origin}});
  }

  navigateToLogin() {
    this.openLogin.emit();
  }

  navigateToTnC(event: any) {
    this.router.navigate([{outlets: {primary: 'legal-pages/terms-of-use', modal: null}}]);
    if (isPlatformBrowser(this.platformId)) {
      const scrollToTop: number = window.setInterval(() => {
        const pos = window.pageYOffset;
        if (pos > 0) {
          window.scrollTo(0, pos - 50); // how far to scroll on each step
        } else {
          window.clearInterval(scrollToTop);
        }
      }, 1);
    }
  }

  navigateToPrivacyPolicy(event: any) {
    this.router.navigate([{outlets: {primary: 'legal-pages/privacy-policy', modal: null}}]);
    if (isPlatformBrowser(this.platformId)) {
      const scrollToTop: number = window.setInterval(() => {
        const pos = window.pageYOffset;
        if (pos > 0) {
          window.scrollTo(0, pos - 50); // how far to scroll on each step
        } else {
          window.clearInterval(scrollToTop);
        }
      }, 1);
    }
  }

  ngOnInit(): void {
    this.buildForms();
    this.setProfile();
    this.addSubscriptions();
  }

  addSubscriptions(): void {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.setProfileToStudent();
        this.becomeTutorButton = true;
      }
    });
  }

  removeSubscriptions(): void {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  ngOnDestroy() {
    this.removeSubscriptions();
  }

  onBecomeTutorClick(): void {
    this.setProfileToTutor();
    this.becomeTutorButton = false;
  }

  setProfileToTutor(): void {
    this.profile = 'tutor';
    this.title = 'Register';
  }

  setProfile(): void {
    if (this.parent !== 'tutor') {
      this.setProfileToStudent();
    } else {
      this.setProfileToTutor();
    }
  }

  setProfileToStudent(): void {
    this.profile = 'student';
    this.title = 'Register';
  }

  buildForms() {
    this.signupform = this.formBuilder.group({
      name: ['', Validators.required],
      mobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern(/^[6-9]\d{9}$/)]]
    });

    this.signupform.valueChanges.subscribe(value => {
      const maskedVal = this.titleCasePipe.transform(value.name);
      if (value.name !== maskedVal) {
        this.signupform.patchValue({ name: maskedVal });
      }
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.signupform.controls; }

  onSubmit() {
    if (this.signupform.valid) {
      this.saveUser();
    }
  }

  private saveUser() {
    const user = new User();
    user.mobile = this.signupform.value.mobile;
    user.name = this.signupform.value.name;
    user.userTypeId = this.profile === 'student' ? 1 : 2;
    this.tregisterService.saveUser(user).subscribe(
      (response) => {
        if (response.status === 'Inactive') {
          this.openVerifyOtpModal();
        } else {
          this.toastService.showError("User already registered. Please Login!");
        }
      });
  }

}
