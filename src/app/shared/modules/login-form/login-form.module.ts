import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginFormComponent } from './components/login-form/login-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ForgotFormComponent } from './components/forgot-form/forgot-form.component';
import { LoginFormLayoutComponent } from './components/login-form-layout/login-form-layout.component';
import { RouterModule } from '@angular/router';
import { SkeletonModule } from '../skeleton/skeleton.module';


@NgModule({
  declarations: [LoginFormComponent, ForgotFormComponent, LoginFormLayoutComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    SkeletonModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [LoginFormLayoutComponent]
})
export class LoginFormModule { }
