import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginFormLayoutComponent } from './login-form-layout.component';

describe('LoginFormLayoutComponent', () => {
  let component: LoginFormLayoutComponent;
  let fixture: ComponentFixture<LoginFormLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginFormLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
