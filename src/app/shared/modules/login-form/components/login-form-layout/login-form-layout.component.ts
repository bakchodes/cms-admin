import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-form-layout',
  templateUrl: './login-form-layout.component.html',
  styleUrls: ['./login-form-layout.component.scss']
})
export class LoginFormLayoutComponent implements OnInit {

  @Output() openRegister = new EventEmitter<any>();
  @Input() origin: string;
  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  navigateToRegister() {
    this.openRegister.emit();
  }

}
