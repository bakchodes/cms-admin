import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { Router } from '@angular/router';
import { MustMatch } from '../../validators/must-match.validator';

@Component({
  selector: 'app-forgot-form',
  templateUrl: './forgot-form.component.html',
  styleUrls: ['./forgot-form.component.scss']
})
export class ForgotFormComponent implements OnInit {

  confirmforgot: FormGroup;
  otpmobNum: FormControl;
  // newpassword: FormControl;
  // confirmPassword: FormControl;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router) {
  }
  ngOnInit() {
    this.confirmforgot = this.formBuilder.group({
      otpmobNum: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }
  get f() { return this.confirmforgot.controls; }

  onSubmit() {
    if (this.confirmforgot.valid) {
      this.confirmforgot.reset();
      return;
    }
  }

}
