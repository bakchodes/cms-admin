import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Utility } from 'src/app/shared/utility';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {

  @Output() openRegister = new EventEmitter<any>();
  @Input() origin: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router) { }

  // public userCredentials: any = {
  //   username: '',
  //   password: '',
  //   grant_type: '',
  //   scope: ''
  // };
  loginForm: FormGroup;
  submitted = false;
  passShow = false;
  otpHide = true;
  clickPassHide = true;
  isPhoneNumber = false;
  selectedCode = '+91';
  returnUrl: string;

  // loginClick() {
  //   this.otpsubmitted = true;
  //   this.submitted = true;
  //   if (this.loginForm.valid) {
  //     this.router.navigate(['/user/tutor-forgot']);
  //   }
  // }


  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


  navigateToRegister() {
    this.openRegister.emit();
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onLoginInput() {
    this.isPhoneNumber = Utility.isPhoneNumber(this.loginForm.value.username);
  }
  // Show enter password div
  onDivClick() {
    this.passShow = !this.passShow;
    this.otpHide = !this.otpHide;
  }

  // Code for forgot password
  onForgotClick() {
    this.submitted = true;
    this.clickPassHide = !this.clickPassHide;
    if (this.loginForm.value.username) {
      this.router.navigate(['/user/tutor-forgot']);
      return;
    }
  }

  openVerifyOtpModal() {
    this.router.navigate([{outlets: {modal: 'modal/verify-otp'}}], {state: {username: this.loginForm.value.username, parent: 'login', origin: this.origin}});
  }
  // Code for OTP modal
  verifyOtpClick() {
    let username = this.loginForm.value.username;
    if (username.match('^[0-9]') && username) {
      if (!username.match('^[0-9]{10}$')) {
        this.f.username.setErrors({mobile: true});
        return;
      }
    } else if (!username.match('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$') && username) {
        this.f.username.setErrors({email: true});
        return;
    }
    this.submitted = false;
    if (this.loginForm.value.username) {
      this.openVerifyOtpModal();
      return;
    }
  }
  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
  }
}
