import { ActionReducerMap } from '@ngrx/store';
import { MasterReducer } from './master/master.reducer';
import { MasterState } from './master/master.state';
import { MasterEffcts } from './master/master.effect';

export interface AppState {
    master: MasterState;
}

export const reducers: ActionReducerMap<AppState> = {
    master: MasterReducer
};

export const effects = [
    MasterEffcts
];
