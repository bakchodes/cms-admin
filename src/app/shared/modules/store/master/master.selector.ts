import { createFeatureSelector, createSelector } from '@ngrx/store';
import { MasterState, BoardState } from './master.state';
import { adapterBoard, adapterStandard, adapterSubject, adapterInteractionType, adapterTuitionType, adapterExam, adapterCourseType, adapterCity, adapterBT, adapterDifficulty, adapterQuestionType, adapterQuestionStatus } from './master.reducer';
import { from } from 'rxjs';
import { map, toArray, mergeMap } from 'rxjs/operators';
import { MasterData } from 'src/app/shared/dtos/master-data';
import { Dictionary } from '@ngrx/entity';

const selectMaster = createFeatureSelector<MasterState>('master');
const selectBoard = (selectMasterState: MasterState) => selectMasterState.boards;
const selectBT = (selectMasterState: MasterState) => selectMasterState.bts;
const selectDifficultyLevel = (selectMasterState: MasterState) => selectMasterState.difficultyLevel;
const selectQuestionType = (selectMasterState: MasterState) => selectMasterState.questionType;
const selectQuestionStatus = (selectMasterState: MasterState) => selectMasterState.questionStatus;
const selectStandards = (selectMasterState: MasterState) => selectMasterState.standards;
const selectSubjects = (selectMasterState: MasterState) => selectMasterState.subjects;
const selectTuitionTypes = (selectMasterState: MasterState) => selectMasterState.tuitionTypes;
const selectInteractionTypes = (selectMasterState: MasterState) => selectMasterState.interactionTypes;
const selectExams = (selectMasterState: MasterState) => selectMasterState.exams;
const selectCourseTypes = (selectMasterState: MasterState) => selectMasterState.courseTypes;
const selectCities = (selectMasterState: MasterState) => selectMasterState.cities;
const selectStates = (selectMasterState: MasterState) => selectMasterState.states;

export const masterSelectorBoards = createSelector(
    selectMaster,
    adapterBoard.getSelectors(selectBoard).selectAll
);

export const masterSelectorBT = createSelector(
    selectMaster,
    adapterBT.getSelectors(selectBT).selectAll
);

export const masterSelectorDifficultyLevel = createSelector(
    selectMaster,
    adapterDifficulty.getSelectors(selectDifficultyLevel).selectAll
);

export const masterSelectorQuestionType = createSelector(
    selectMaster,
    adapterQuestionType.getSelectors(selectQuestionType).selectAll
);

export const masterSelectorQuestionStatus = createSelector(
    selectMaster,
    adapterQuestionStatus.getSelectors(selectQuestionStatus).selectAll
);

export const masterSelectorStandards = createSelector(
    selectMaster,
    adapterStandard.getSelectors(selectStandards).selectAll
);

export const masterSelectorSubjects = createSelector(
    selectMaster,
    adapterSubject.getSelectors(selectSubjects).selectAll
);

export const masterSelectorTuitionTypes = createSelector(
    selectMaster,
    adapterTuitionType.getSelectors(selectTuitionTypes).selectAll
);

export const masterSelectorInteractionTypes = createSelector(
    selectMaster,
    adapterInteractionType.getSelectors(selectInteractionTypes).selectAll
);

export const masterSelectorExams = createSelector(
    selectMaster,
    adapterExam.getSelectors(selectExams).selectAll
);

export const masterSelectorCourseTypes = createSelector(
    selectMaster,
    adapterCourseType.getSelectors(selectCourseTypes).selectAll
);

export const masterSelectorCities = createSelector(
    selectMaster,
    adapterCity.getSelectors(selectCities).selectAll
);

export const masterSelectorCitiesDictionary = createSelector(
    selectMaster,
    adapterCity.getSelectors(selectCities).selectEntities
);

export const masterSelectorStatesDictionary = createSelector(
    selectMaster,
    adapterCity.getSelectors(selectStates).selectEntities
);

export const masterSelectorCitiesIds = createSelector(
    selectMaster,
    adapterCity.getSelectors(selectCities).selectIds
);

export const masterSelectorCitiesByIds = createSelector(
    selectMaster,
    masterSelectorCitiesDictionary,
    (state: MasterState, data: Dictionary<MasterData>, ids: number[]) => ids.map(id => data[id])
);

export const masterSelectorStatesIds = createSelector(
    selectMaster,
    adapterCity.getSelectors(selectStates).selectIds
);

export const masterSelectorStatesByIds = createSelector(
    selectMaster,
    masterSelectorCitiesDictionary,
    (state: MasterState, data: Dictionary<MasterData>, ids: number[]) => ids.map(id => data[id])
);

export const masterSelectorStandardsDictionary = createSelector(
    selectMaster,
    adapterStandard.getSelectors(selectStandards).selectEntities
);

export const masterSelectorBoardsDictionary = createSelector(
    selectMaster,
    adapterBoard.getSelectors(selectBoard).selectEntities
);

export const masterSelectorBTDictionary = createSelector(
    selectMaster,
    adapterBT.getSelectors(selectBT).selectEntities
);

export const masterSelectorSubjectsDictionary = createSelector(
    selectMaster,
    adapterStandard.getSelectors(selectSubjects).selectEntities
);

export const masterSelectorCurrentState = createSelector(
    selectMaster,
    (state: MasterState) => state.currentState
);

export const masterSelectorCurrentStates = createSelector(
    selectMaster,
    (state: MasterState) => state.currentState
);

export const masterSelectorCurrentCities = createSelector(
    selectMaster,
    (state: MasterState) => state.currentState
);
