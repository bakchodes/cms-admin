import { MasterDataService } from 'src/app/shared/services/master-data/master-data.service';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { map, filter, withLatestFrom, switchMap, combineLatest } from 'rxjs/operators';
import * as MasterActions from './master.action';
import { Injectable } from '@angular/core';
import { AppState } from '..';
import { Store, select } from '@ngrx/store';
import { masterSelectorBoards, masterSelectorStandards, masterSelectorSubjects, 
    masterSelectorTuitionTypes, masterSelectorInteractionTypes, masterSelectorExams, masterSelectorCourseTypes, masterSelectorCurrentState, masterSelectorCities, masterSelectorCurrentCities, masterSelectorCitiesByIds, masterSelectorCitiesIds, masterSelectorStatesIds, masterSelectorBT, masterSelectorDifficultyLevel, masterSelectorQuestionType, masterSelectorQuestionStatus } from './master.selector';
import { of } from 'rxjs';

@Injectable()
export class MasterEffcts {

    loadBoards$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadBoards),
            withLatestFrom(this.store.pipe(select(masterSelectorBoards))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getBoards$().pipe(
                map((boards) => MasterActions.loadBoardsSuccess({boards}))
            ))
        )
    );

    loadBT$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadBT),
            withLatestFrom(this.store.pipe(select(masterSelectorBT))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getBT$().pipe(
                map((bts) => MasterActions.loadBTSuccess({bts}))
            ))
        )
    );

    loadDifficultyLevel$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadDifficultyLevel),
            withLatestFrom(this.store.pipe(select(masterSelectorDifficultyLevel))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getDifficultyLevel$().pipe(
                map((difficultyLevel) => MasterActions.loadDifficultyLevelSuccess({difficultyLevel}))
            ))
        )
    );

    loadQuestionType$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadQuestionType),
            withLatestFrom(this.store.pipe(select(masterSelectorQuestionType))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getQuestionType$().pipe(
                map((questionType) => MasterActions.loadQuestionTypeSuccess({questionType}))
            ))
        )
    );

    loadQuestionStatus$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadQuestionStatus),
            withLatestFrom(this.store.pipe(select(masterSelectorQuestionStatus))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getQuestionStatus$().pipe(
                map((questionStatus) => MasterActions.loadQuestionStatusSuccess({questionStatus}))
            ))
        )
    );

    loadStandards$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadStandards),
            withLatestFrom(this.store.pipe(select(masterSelectorStandards))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getStandards$().pipe(
                map((standards) => MasterActions.loadStandardsSuccess({standards}))
            ))
        )
    );

    loadSubjects$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadSubjects),
            withLatestFrom(this.store.pipe(select(masterSelectorSubjects))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getSubjects$().pipe(
                map((subjects) => MasterActions.loadSubjectsSuccess({subjects}))
            ))
        )
    );

    loadTuitionTypes$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadTuitionTypes),
            withLatestFrom(this.store.pipe(select(masterSelectorTuitionTypes))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getTuitionTypes$().pipe(
                map((tuitionTypes) => MasterActions.loadTuitionTypesSuccess({tuitionTypes}))
            ))
        )
    );

    loadInteractionTypes$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadInteractionTypes),
            withLatestFrom(this.store.pipe(select(masterSelectorInteractionTypes))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getInteractionTypes$().pipe(
                map((interactionTypes) => MasterActions.loadInteractionTypesSuccess({interactionTypes}))
            ))
        )
    );

    loadExams$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadExams),
            withLatestFrom(this.store.pipe(select(masterSelectorExams))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getExams$().pipe(
                map((exams) => MasterActions.loadExamsSuccess({exams}))
            ))
        )
    );

    loadCourseTypes$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadCourseTypes),
            withLatestFrom(this.store.pipe(select(masterSelectorCourseTypes))),
            filter(([action, data]) => data.length === 0),
            switchMap(() => this.mdService.getCourseTypes$().pipe(
                map((courseTypes) => MasterActions.loadCourseTypesSuccess({courseTypes}))
            ))
        )
    );

    loadCitiesByState$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadCitiesByState),
            withLatestFrom(this.actions$, this.store.pipe(select(masterSelectorCurrentState)), this.store.pipe(select(masterSelectorCities)), (stateValObj, action,stateVal, citiesVal) => {
                return {action:stateValObj, state: stateVal, cities: citiesVal};
            }),
            filter((data) => data.cities.length === 0),
            switchMap((data) => this.mdService.getCitiesByState$(data.action.stateId).pipe(
                map((cities) => MasterActions.loadCitiesByStateSuccess({cities}))
            ))
        )
    );

    loadCitiesByIds$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadCitiesByIds),
            withLatestFrom(this.store.pipe(select(masterSelectorCitiesIds))),
            filter(([action, data]) => this.filterData((data as number[]),action.ids)),
            switchMap(([action]) => this.mdService.getCitiesByIds$(action.ids).pipe(
                map((cities) => MasterActions.loadCitiesByIdsSuccess({cities}))
            ))
        )
    );

    loadStatesByIds$ = createEffect(() =>
        this.actions$.pipe(
            ofType(MasterActions.loadStatesByIds),
            withLatestFrom(this.store.pipe(select(masterSelectorStatesIds))),
            filter(([action, data]) => this.filterData((data as number[]),action.ids)),
            switchMap(([action]) => this.mdService.getStatesByIds$(action.ids).pipe(
                map((states) => MasterActions.loadStatesByIdsSuccess({states}))
            ))
        )
    );

    private filterData(data:number[],actionData:number[]){
        var notfound = true;
        for (var i = 0; i < actionData.length; i++) {
            if (data.indexOf(actionData[i]) > -1) {
                notfound = false;
                break;
            }
        }
        return notfound;
    }

    constructor(
        private store: Store<AppState>,
        private actions$: Actions,
        private mdService: MasterDataService
    ) { }
}
