import { createAction, props } from '@ngrx/store';

import { MasterData } from 'src/app/shared/dtos/master-data';

export const loadBoards = createAction('[Master] Load boards');
export const loadBoardsSuccess = createAction('[Master] Load boards success', props<{boards: MasterData[]}>());
export const loadBT = createAction('[Master] Load blooms Taxonomy');
export const loadBTSuccess = createAction('[Master] Load Blooms Taxonomy success', props<{bts: MasterData[]}>());
export const loadDifficultyLevel = createAction('[Master] Load difficulty level');
export const loadDifficultyLevelSuccess = createAction('[Master] Load difficulty level success', props<{difficultyLevel: MasterData[]}>());
export const loadQuestionType = createAction('[Master] Load question type');
export const loadQuestionTypeSuccess = createAction('[Master] Load question type success', props<{questionType: MasterData[]}>());
export const loadQuestionStatus = createAction('[Master] Load question status');
export const loadQuestionStatusSuccess = createAction('[Master] Load question status success', props<{questionStatus: MasterData[]}>());
export const loadStandards = createAction('[Master] Load standards');
export const loadStandardsSuccess = createAction('[Master] Load standards success', props<{standards: MasterData[]}>());
export const loadSubjects = createAction('[Master] Load subjects');
export const loadSubjectsSuccess = createAction('[Master] Load subjects success', props<{subjects: MasterData[]}>());
export const loadTuitionTypes = createAction('[Master] Load tuition types');
export const loadTuitionTypesSuccess = createAction('[Master] Load tuition types success', props<{tuitionTypes: MasterData[]}>());
export const loadInteractionTypes = createAction('[Master] Load interaction types');
export const loadInteractionTypesSuccess = createAction('[Master] Load interaction types success',
props<{interactionTypes: MasterData[]}>());
export const loadExams = createAction('[Master] Load exams');
export const loadExamsSuccess = createAction('[Master] Load exams success', props<{exams: MasterData[]}>());
export const loadCourseTypes = createAction('[Master] Load course types');
export const loadCourseTypesSuccess = createAction('[Master] Load course types success', props<{courseTypes: MasterData[]}>());
export const loadCitiesByState = createAction('[Master] Load cities by state', props<{stateId: number}>());

export const loadCitiesByIds= createAction('[Master] Load cities by Ids', props<{ids: number[]}>());
export const loadCitiesByIdsSuccess = createAction('[Master] Load cities by ids success', props<{cities: MasterData[]}>());
export const loadStatesByIds= createAction('[Master] Load states by Ids', props<{ids: number[]}>());
export const loadStatesByIdsSuccess = createAction('[Master] Load states by ids success', props<{states: MasterData[]}>());



export const loadCitiesByStateSuccess = createAction('[Master] Load cities by state success', props<{cities: MasterData[]}>());
export const updateCurrentState = createAction('[Master] Update current state', props<{currentStateVal: MasterData}>());