import { EntityState } from '@ngrx/entity';

import { MasterData } from 'src/app/shared/dtos/master-data';


export interface MasterState {
    states: StateState;
    boards: BoardState;
    difficultyLevel: DifficultyLevelState;
    bts: BTState;
    questionType: QuestionTypeState;
    questionStatus: QuestionStatusState;
    standards: StandardState;
    subjects: SubjectState;
    exams: ExamState;
    tuitionTypes: TuitionTypeState;
    interactionTypes: InteractionTypeState;
    courseTypes: CourseTypeState;
    currentState: MasterData;
    cities: CityState;
}

export interface BoardState extends EntityState<MasterData> {

}

export interface BTState extends EntityState<MasterData> {

}
export interface DifficultyLevelState extends EntityState<MasterData> {

}

export interface QuestionTypeState extends EntityState<MasterData> {

}

export interface QuestionStatusState extends EntityState<MasterData> {

}

export interface StandardState extends EntityState<MasterData> {

}

export interface SubjectState extends EntityState<MasterData> {

}

export interface ExamState extends EntityState<MasterData> {

}

export interface TuitionTypeState extends EntityState<MasterData> {

}

export interface InteractionTypeState extends EntityState<MasterData> {

}

export interface CourseTypeState extends EntityState<MasterData> {

}

export interface CityState extends EntityState<MasterData> {
}

export interface StateState extends EntityState<MasterData> {
}