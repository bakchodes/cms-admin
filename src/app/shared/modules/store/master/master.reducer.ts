import { createEntityAdapter } from '@ngrx/entity';
import { MasterData } from 'src/app/shared/dtos/master-data';
import { BoardState, StandardState, MasterState, SubjectState, TuitionTypeState,
    InteractionTypeState, ExamState, CourseTypeState, CityState, StateState, BTState, DifficultyLevelState, QuestionTypeState, QuestionStatusState } from './master.state';
import { createReducer, Action, on } from '@ngrx/store';
import * as MasterActions from './master.action';

export const adapterBoard = createEntityAdapter<MasterData>();
export const adapterBT = createEntityAdapter<MasterData>();
export const adapterDifficulty = createEntityAdapter<MasterData>();
export const adapterQuestionType = createEntityAdapter<MasterData>();
export const adapterQuestionStatus = createEntityAdapter<MasterData>();
export const adapterStandard = createEntityAdapter<MasterData>();
export const adapterSubject = createEntityAdapter<MasterData>();
export const adapterTuitionType = createEntityAdapter<MasterData>();
export const adapterInteractionType = createEntityAdapter<MasterData>();
export const adapterExam = createEntityAdapter<MasterData>();
export const adapterCourseType = createEntityAdapter<MasterData>();
export const adapterCity = createEntityAdapter<MasterData>();

const boardInitialState: BoardState = adapterBoard.getInitialState();
const btInitialState: BTState = adapterBT.getInitialState();
const difficulyLevelInitialState: DifficultyLevelState = adapterDifficulty.getInitialState();
const questionTypeInitialState: QuestionTypeState = adapterQuestionType.getInitialState();
const questionStatusInitialState: QuestionStatusState = adapterQuestionStatus.getInitialState();
const standardInitialState: StandardState = adapterStandard.getInitialState();
const subjectInitialState: SubjectState = adapterSubject.getInitialState();
const tuitionTypeInitialState: TuitionTypeState = adapterTuitionType.getInitialState();
const interactionTypeInitialState: InteractionTypeState = adapterInteractionType.getInitialState();
const examInitialState: ExamState = adapterExam.getInitialState();
const courseTypeInitialState: CourseTypeState = adapterCourseType.getInitialState();
const cityInitialState: CityState = adapterCity.getInitialState();
const stateInitialState: StateState = adapterCity.getInitialState();

const initialState: MasterState = {
    boards: boardInitialState,
    bts: btInitialState,
    difficultyLevel: difficulyLevelInitialState,
    questionType:questionTypeInitialState,
    questionStatus: questionStatusInitialState,
    standards: standardInitialState,
    subjects: subjectInitialState,
    tuitionTypes: tuitionTypeInitialState,
    interactionTypes: interactionTypeInitialState,
    exams: examInitialState,
    courseTypes: courseTypeInitialState,
    cities: cityInitialState,
    states:stateInitialState,
    currentState: null
};

const reducer = createReducer(
    initialState,
    on(MasterActions.loadBoardsSuccess, (state, {boards}) => {
        return { ...state, boards: adapterBoard.addMany(boards, state.boards) };
    }),
    on(MasterActions.loadBTSuccess, (state, {bts}) => {
        return { ...state, bts: adapterBT.addMany(bts, state.bts) };
    }),
    on(MasterActions.loadDifficultyLevelSuccess, (state, {difficultyLevel}) => {
        return { ...state, difficultyLevel: adapterDifficulty.addMany(difficultyLevel, state.difficultyLevel) };
    }),
    on(MasterActions.loadQuestionTypeSuccess, (state, {questionType}) => {
        return { ...state, questionType: adapterQuestionType.addMany(questionType, state.questionType) };
    }),
    on(MasterActions.loadQuestionStatusSuccess, (state, {questionStatus}) => {
        return { ...state, questionStatus: adapterQuestionStatus.addMany(questionStatus, state.questionStatus) };
    }),
    on(MasterActions.loadStandardsSuccess, (state, {standards}) => {
        return { ...state, standards: adapterStandard.addMany(standards, state.standards) };
    }),
    on(MasterActions.loadSubjectsSuccess, (state, {subjects}) => {
        return { ...state, subjects: adapterSubject.addMany(subjects, state.subjects) };
    }),
    on(MasterActions.loadTuitionTypesSuccess, (state, {tuitionTypes}) => {
        return { ...state, tuitionTypes: adapterTuitionType.addMany(tuitionTypes, state.tuitionTypes) };
    }),
    on(MasterActions.loadInteractionTypesSuccess, (state, {interactionTypes}) => {
        return { ...state, interactionTypes: adapterInteractionType.addMany(interactionTypes, state.interactionTypes) };
    }),
    on(MasterActions.loadExamsSuccess, (state, {exams}) => {
        return { ...state, exams: adapterExam.addMany(exams, state.exams) };
    }),
    on(MasterActions.loadCourseTypesSuccess, (state, {courseTypes}) => {
        return { ...state, courseTypes: adapterCourseType.addMany(courseTypes, state.courseTypes) };
    }),
    on(MasterActions.updateCurrentState, (state, {currentStateVal}) => {
        return { ...state, currentState: currentStateVal};
    }),
    on(MasterActions.loadCitiesByStateSuccess, (state, {cities}) => {
        return { ...state, cities: adapterCity.addMany(cities, state.cities) };
    }),
    on(MasterActions.loadCitiesByIdsSuccess, (state, {cities}) => {
        return { ...state, cities: adapterCity.addMany(cities, state.cities ) };
    }),
    on(MasterActions.loadStatesByIdsSuccess, (state, {states}) => {
        return { ...state, states: adapterCity.addMany(states, state.states ) };
    }),

);

export function MasterReducer(state: MasterState, action: Action) {
    return reducer(state, action);
}
