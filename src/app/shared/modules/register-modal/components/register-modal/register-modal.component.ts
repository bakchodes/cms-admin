import { Component, OnInit } from '@angular/core';
import { ModalViewComponent } from 'src/app/modules/app/components/modalview/modalview.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.scss']
})
export class RegisterModalComponent implements OnInit {

  state: { parent: string };
  constructor(
    private modalViewComponent: ModalViewComponent,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.setState();
  }

  setState(): void {
    this.state = window.history.state;
  }

  public dismiss(context?: string) {
    this.modalViewComponent.closeCurrentModal();
  }

  public navigateToLogin() {
    this.router.navigate([{outlets: {modal: 'modal/login'}}], { replaceUrl: true });
  }

}
