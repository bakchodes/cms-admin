import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterModalComponent } from './components/register-modal/register-modal.component';

const routes: Routes = [
  {path: '', component: RegisterModalComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterModalRoutingModule { }
