import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterModalRoutingModule } from './register-modal-routing.module';
import { RegisterFormModule } from '../register-form/register-form.module';
import { RegisterModalComponent } from './components/register-modal/register-modal.component';


@NgModule({
  declarations: [RegisterModalComponent],
  imports: [
    CommonModule,
    RegisterModalRoutingModule,
    RegisterFormModule
  ]
})
export class RegisterModalModule { }
