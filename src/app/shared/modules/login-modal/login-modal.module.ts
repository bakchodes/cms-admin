import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginModalComponent } from './components/login-modal/login-modal.component';
import { LoginFormModule } from '../login-form/login-form.module';
import { RegisterFormModule } from '../register-form/register-form.module';
import { LoginModalRoutingModule } from './login-modal-routing.module';


@NgModule({
  declarations: [LoginModalComponent],
  imports: [
    CommonModule,
    LoginFormModule,
    LoginModalRoutingModule
  ]
})
export class LoginModalModule { }
