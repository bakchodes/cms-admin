import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalViewComponent } from 'src/app/modules/app/components/modalview/modalview.component';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})
export class LoginModalComponent implements OnInit {

  constructor(private router: Router, private modalViewComponent: ModalViewComponent) { }

  ngOnInit() {
  }

  public dismiss(context?: string) {
    this.modalViewComponent.closeCurrentModal();
  }

  navigateToRegister() {
    this.router.navigate([{ outlets: { modal: 'modal/register' } }], { replaceUrl: true, state: { parent: 'common' } });
  }

}
