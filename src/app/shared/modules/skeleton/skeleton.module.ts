import {NgModule} from '@angular/core';
import {CommonModule } from '@angular/common';
import {SkeletonRoutingModule} from './skeleton-routing.module';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {LeftfixedComponent} from './components/leftfixed/leftfixed.component';
import { CpleftsideComponent} from './components/cpleftside/cpleftside.component';
import { NgbModule, NgbCollapse } from '@ng-bootstrap/ng-bootstrap';
import { CountrycodeComponent } from './components/countrycode/countrycode.component';

@NgModule({
declarations: [HeaderComponent, FooterComponent, LeftfixedComponent, CpleftsideComponent, CountrycodeComponent],
imports: [
CommonModule,
SkeletonRoutingModule,
NgbModule
],
exports: [
HeaderComponent,
FooterComponent,
LeftfixedComponent,
CpleftsideComponent,
CountrycodeComponent
],
providers: [ NgbCollapse ],
})
export class SkeletonModule {

}
