import { Component, OnInit } from '@angular/core';
import countryCodes from './../../../../dtos/mock/countrycodes.json';

@Component({
  selector: 'app-countrycode',
  templateUrl: './countrycode.component.html',
  styleUrls: ['./countrycode.component.scss']
})
export class CountrycodeComponent implements OnInit {

  constructor() { }

  // code for countrycode
  countryCodes: any = countryCodes;
  selectedCode = '+91';
  onCountrySelect(countryCode: any): void {
    this.selectedCode = countryCode.dialling_code;
  }

  ngOnInit() {
  }

}
