import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { ToastService } from 'src/app/shared/services/toast/toast.service';
import { Observable, Subscription } from 'rxjs';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Input() heading: string;

  isLoggedIn: boolean;
  private isLoggedInSubscription$: Subscription;
  public isCollapsed = false;
  // menuIcon = true;
  closeIcon = false;
  // menuclickHide = true;
  showpanel = false;
  hidepanel = false;
  fadeBG = false;

  private currentRoute: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.addSubscriptions();
    // HEADER STICKY JS
    // tslint:disable-next-line: only-arrow-functions
    window.onscroll = function () { myFunction(); };
    // tslint:disable-next-line: prefer-const
    let header = document.getElementById('navbar-fixed');
    const sticky = header.offsetTop;
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add('sticky');
      } else {
        header.classList.remove('sticky');
      }
    }


    this.currentRoute = this.router.url;
  }

  ngOnDestroy(): void {
    this.removeSubscriptions();
  }

  private addSubscriptions() {
    this.isLoggedInSubscription$ = this.authService.isLoggedIn().subscribe((loginStatus) => {
      this.isLoggedIn = loginStatus;
    });
  }

  private removeSubscriptions() {
    this.isLoggedInSubscription$.unsubscribe();
  }

  mobMenu() {
    // this.menuIcon = !this.menuIcon;
    // this.menuclickHide = !this.menuclickHide;
    // this.fadeBG = !this.fadeBG;
    if (this.showpanel) {
      this.showpanel = false;
      this.hidepanel = !this.hidepanel;
    } else {
      this.showpanel = true;
      this.hidepanel = false;
    }
  }

  navigateToRegister() {
    this.router.navigate(['/register'], { state: { parent: 'common' } });
  }
  navigateToLogin() {
    this.router.navigate(['/login'], {state: {origin: this.currentRoute}});
  }

  logOut() {
    this.authService.removeAuthorizationToken();
    this.router.navigate(['/']);
  }

}
