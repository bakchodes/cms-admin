import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpleftsideComponent } from './cpleftside.component';

describe('CpleftsideComponent', () => {
  let component: CpleftsideComponent;
  let fixture: ComponentFixture<CpleftsideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpleftsideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpleftsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
