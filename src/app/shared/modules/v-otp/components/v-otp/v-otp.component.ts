import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Utility } from '../../../../utility';
import { User } from '../../../../dtos/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { VOtpService } from '../../services/v-otp.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { ToastService } from 'src/app/shared/services/toast/toast.service';
import { ModalViewComponent } from 'src/app/modules/app/components/modalview/modalview.component';

@Component({
  selector: 'app-v-otp',
  templateUrl: './v-otp.component.html',
  styleUrls: ['./v-otp.component.scss'],
  providers: [VOtpService]
})
export class VOtpComponent implements OnInit {

  username: string;
  parent: string;
  private origin: string;
  state: { parent: string, username: string, origin: string };
  h1: string;
  h2: string;
  h3: string;
  verifyform: FormGroup;


  public countdownConfig: any;
  public isOtpResendEnabled = false;

  constructor(
    private formBuilder: FormBuilder,
    public activeModal: NgbActiveModal,
    private votpService: VOtpService,
    private authService: AuthService,
    private toastService: ToastService,
    private modalViewComponent: ModalViewComponent,
    private router: Router
  ) {


  }
  User: any = User;
  ngOnInit() {
    this.state = window.history.state;
    this.username = this.state.username;
    this.parent = this.state.parent;
    this.buildForms();
    this.setHeadlineText();
    if (this.parent === 'login') {
      this.sendLoginOtp();
    }

    // Start Countdown
    this.startCountdown();
  }

  private setOrigin(url: string) {
    if (url) {
      if (url.includes("(modal:modal/verify-otp)")) {
        return url.replace("(modal:modal/verify-otp)", '')
      } else {
        return url;
      }
    }
    return '';
  }

  private buildForms() {
    this.verifyform = this.formBuilder.group({
      username: [this.username],
      otp: ['', [Validators.required, Validators.minLength(4)]],
    });
  }

  private setHeadlineText() {
    if (Utility.isPhoneNumber(this.username)) {
      this.h1 = 'Verify Mobile Number';
      this.h2 = 'An OTP has been sent to your mobile number';
      this.h3 = 'Mobile Number';
    } else {
      this.h1 = 'Verify Email';
      this.h2 = 'An OTP has been sent to your email';
      this.h3 = 'Email';
    }
  }

  public verifyOtp() {
    this.votpService.verifyOtp(this.verifyform.value.otp, this.verifyform.value.username).subscribe(
      (data) => {
          this.authService.setAuthorizationToken(data.credentials.access_token);
          this.authService.setRefreshToken(data.credentials.refresh_token);
          switch (this.state.origin) {
            case 'page':
              this.router.navigate([{ outlets: { primary: '/', modal: null } }]);
              break;
            case 'modal':
              this.router.navigate([{ outlets: { modal: null } }]);
              break;
          }
          // TODO: uncomment when dashboard ready
          // switch (this.parent) {
          //   case 'login':
          //     this.router.navigate([{outlets: {primary: '/create-profile/step-1', modal: null}}], { replaceUrl: true });
          //     // TODO: move to dashboard instead of create profile
          //     break;
          //   case 'register':
          //     this.router.navigate([{outlets: {primary: '/create-profile/step-1', modal: null}}], { replaceUrl: true });
          //     break;
          // }
        
      });
  }

  public dismissAllModals() {
    this.modalViewComponent.closeAllModals();
  }

  public dismiss(context: string) {
    this.modalViewComponent.closeCurrentModal();
  }

  private sendLoginOtp() {
    this.votpService.sendLoginOtp(this.verifyform.value.username).subscribe(
      (data) => {
          this.toastService.showSuccess("OTP sent successfully!");
      });
  }

  public resendOtp() {
    if (!this.isOtpResendEnabled) {
      this.toastService.showCustomToast('Wait for some time!');
      return;
    }

    this.votpService.resendOtp(this.verifyform.value.username).subscribe(
      (data) => {
          this.toastService.showSuccess("OTP sent successfully!");
      });

    // Enable OT button again
    this.isOtpResendEnabled = false;
    this.startCountdown();
  }

  private startCountdown() {
    this.countdownConfig = {
      leftTime: 30,
      format: 'mm:ss'
    };
  }

  public handleCountdownEvent(event) {
    if (event.action === 'done') {
      this.isOtpResendEnabled = true;
    }
  }

}
