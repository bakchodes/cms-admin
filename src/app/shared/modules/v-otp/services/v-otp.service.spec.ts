import { TestBed } from '@angular/core/testing';

import { VOtpService } from './v-otp.service';

describe('VOtpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VOtpService = TestBed.get(VOtpService);
    expect(service).toBeTruthy();
  });
});
