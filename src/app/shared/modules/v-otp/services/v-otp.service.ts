import { Injectable } from '@angular/core';
import { NetworkService } from 'src/app/shared/services/network/network.service';
import { Api } from 'src/app/shared/constants/api';
import { SendOtpResponse } from 'src/app/shared/dtos/response/send-otp-response';
import { VerifyOtpResponse } from 'src/app/shared/dtos/response/verify-otp-response';

@Injectable()
export class VOtpService {

  constructor(private networkService: NetworkService) { }

  verifyOtp(otp: string, username: string) {
    return this.networkService.put<VerifyOtpResponse>(Api.VERIFY_OTP(otp, username), null);
  }

  resendOtp(username: string) {
    return this.networkService.put<SendOtpResponse>(Api.RESEND_OTP(username));
  }

  sendLoginOtp(username: string) {
    return this.networkService.post<SendOtpResponse>(Api.SEND_LOGIN_OTP(username));
  }
}
