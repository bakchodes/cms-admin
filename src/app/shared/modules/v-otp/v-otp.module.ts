import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VOtpComponent } from '../v-otp/components/v-otp/v-otp.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { VotpRoutingModule } from './v-top-routing.module';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
    declarations: [VOtpComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        VotpRoutingModule,
        NgbModule,
        CountdownModule
    ],
    providers: [
        NgbActiveModal
    ],
    entryComponents: [VOtpComponent],
    exports: [
        VOtpComponent
    ]
})
export class VOtpModule {

}
