import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VOtpComponent } from './components/v-otp/v-otp.component';

const routes: Routes = [
  { path: '', component: VOtpComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VotpRoutingModule { }
