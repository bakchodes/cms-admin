export class User {
    userTypeId: number;
    name: string;
    mobile: number;
    password: string;
    username: string;
    otpverify: string;
}
