
export interface QuestionRequest {
    questionIds:number[]
    conceptIds: number[]
    start: number
    size: number
  }