import { MasterData } from '../master-data';
import { Choice } from './choice';

export interface Question {
    id: number
    question: string
    choices: Choice[]
    solution:string
    questionType: MasterData
    questionStatus: MasterData
    bloomsTaxonomy: MasterData
    concepts:MasterData
    difficultyLevel: MasterData
  }