export interface Choice {
    id: number
    choice: string
    isCorrect: boolean
  }