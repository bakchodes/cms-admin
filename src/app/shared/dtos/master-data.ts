export interface MasterData {
    id: number;
    name: string;
}

export interface MasterDataMap {
    [key: string]: MasterData
}
