export class BaseResponse<T> {
    private status: string;
    private data: T;
    private statusCode: number;
    private message: string;
    
    constructor(
        status?: string,
        data?: T,
        statusCode?: number,
        message?: string
    ) {
        this.status = status;
        this.data = data;
        this.statusCode = statusCode;
        this.message = message;
    }

    public getInterfaceSkeleton() {
        return {
            status: this.status,
            data: this.data,
            statusCode: this.statusCode,
            message: this.message
        }
    }
    
}
