import { environment } from 'src/environments/environment';

export class Api {
  public static BASE_URL = environment.baseUrl;

  //questions
  public static SAVE_QUESTION = "cms/question/save";
  public static UPDATE_QUESTION = "cms/question/update";
  public static SEARCH_QUESTIONS = "cms/question/search";

  //storage
  public static UPLOAD_FILE = "cms/storage/upload";

  public static SAVE_USER = 'user/save';
  public static GET_USER_STATUS = (ssoid: string) =>
    `user/getuserstatus?ssoid=${ssoid}`;
  public static GET_SYLLABUS_NODE = (name: string) =>
    `community/api/syllabus-node/${name}`;
  public static GET_NAVIGATION = () =>
    `community/api/navigation`;
  public static DOWNLOAD_FILE = (name: string, node: string) =>
    'community/secured/url?objectkey=' + name + '&node=' + node;
  public static VERIFY_OTP = (otp: string, ssoid: string) =>
    `user/verifyuser?otp=${otp}&ssoid=${ssoid}`;
  public static RESEND_OTP = (ssoid: string) => `user/resendotp?ssoid=${ssoid}`;
  public static SEND_LOGIN_OTP = (ssoid: string) =>
    `user/sendloginotp?ssoid=${ssoid}`;

  // Create Profile Services
  public static TUTOR_CURRENT_PROFILE = 'tutor/current';
  public static TUTOR_CREATE_PROFILE = 'tutor/save';
  public static TUTOR_PROFILE_DATA = `tutor/tutor-by-userid`;
  public static TUTOR_BY_ID = (id: number) => `tutor/tutor-by-id?id=${id}`;

  // Storage
  public static STORAGE_PRESIGNED_DOWNLOAD = (objectkey, foldername) =>
    `utility/storage/presignedurl?objectkey=${objectkey}&foldername=${foldername}`;
  public static STORAGE_PRESIGNED_UPLOAD = (filename, foldername) =>
    `utility/storage/presignedurl?filename=${filename}&foldername=${foldername}`;

  //payment (order)
  public static VERIFY_PAYMENT = 'order/verify';
  public static CREATE_ORDER = 'order/create';

  //payment (cart)
  public static FETCH_CART = 'cart/detail'
  public static REMOVE_CART_ITEM = (id:number) => `cart/item/${id}`;
  public static EMPTY_CART = 'cart';
  


  
  // Residential Data - Create Profile - Step 1 
  public static TUTOR_FORM_OPTIONS_COUNTRY_GETALL = 'tutor/country/all';
  public static TUTOR_FORM_OPTIONS_STATE_GETALL_BY_COUNTRY = (countryid) => 
  `tutor/state/find-by-country-id?countryid=${countryid}`;
  public static TUTOR_FORM_OPTIONS_CITY_GETALL_BY_STATE = (stateid) =>
  `tutor/city/find-by-state-id?stateid=${stateid}`;

  // Academics Data - Create Profile - Step 1
  public static TUTOR_FORM_OPTIONS_EDUCATION_GETALL = 'tutor/education/all';
  public static TUTOR_FORM_OPTIONS_COURSE_GETALL = 'tutor/course/all';
  public static TUTOR_FORM_OPTIONS_SPECIALIZATION_GETALL = 'tutor/specialization/all';
  public static TUTOR_FORM_OPTIONS_UNIVERSITY_GETALL = 'tutor/university/all';

  // Teaching Experience ENdpoints - Create profile - Step 2
  public static TUTOR_FORM_OPTIONS_STANDARD_GETALL = 'cms/standard/all';
  public static TUTOR_FORM_OPTIONS_SUBJECT_GETALL = 'cms/subject/all';
  public static TUTOR_FORM_OPTIONS_BOARD_GETALL = 'cms/board/all';
  public static TUTOR_FORM_OPTIONS_BT_GETALL = 'cms/bt/all';
  public static TUTOR_FORM_OPTIONS_DL_GETALL = 'cms/difficulty-level/all';
  public static TUTOR_FORM_OPTIONS_QUESTION_TYPE_GETALL = 'cms/question-type/all';
  public static TUTOR_FORM_OPTIONS_QUESTION_STATUS_GETALL = 'cms/question-status/all';
  public static TUTOR_FORM_OPTIONS_LANGUAGE_GETALL = 'tutor/language/all';
  public static TUTOR_FORM_OPTIONS_REWARDINGS_GETALL = 'tutor/rewarding/all';
  public static TUTOR_FORM_OPTIONS_DISABILITY_GETALL = 'tutor/disability/all';
  public static TUTOR_FORM_OPTIONS_COURSETYPE_GETALL = 'tutor/course-type/all';
  
}
