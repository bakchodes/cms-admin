export class Utility {
    public static isPhoneNumber(value: string): boolean {
        const reg = new RegExp('^[0-9]+$');
        if (reg.test(value)) {
            return true;
        } else {
            return false;
        }
    }
}
