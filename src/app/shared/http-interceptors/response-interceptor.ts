import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Api } from '../constants/api';
import { LoggerService } from '../services/logger/logger.service';
import { tap, map, catchError, retry } from 'rxjs/operators';
import { BaseResponse } from '../dtos/response/base-response';
import { throwError } from 'rxjs';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {


    
    constructor(
        private logger: LoggerService
    ) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        return next.handle(req).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    let baseResponse = new BaseResponse();
                    const mandatoryKeys = Object.keys(baseResponse.getInterfaceSkeleton());

                    if (this.isVaildBaseResponse(mandatoryKeys, event.body)) {
                        if (event.body.status !== 'success') {
                            // Put your toast message in here
                        }
                    } else {
                        throw new HttpErrorResponse({
                            error: 'Some error occured! Unprocessed entity!',
                            status: 422,
                            statusText: 'Unprocessed entity!'
                        });
                    }
                    const modEvent = event.clone({ body: event.body.data });
                    return modEvent;
                }
            })
        );
    }

    private isVaildBaseResponse(mandatoryKeys, response) {
        let responseKeys = Object.keys(response);
        for (let i=0; i<mandatoryKeys.length; i++) {
            if (responseKeys.indexOf(mandatoryKeys[i]) == -1) return false;
        }
        return true;
    }
}