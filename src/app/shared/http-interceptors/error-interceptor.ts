import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { finalize, catchError } from 'rxjs/operators';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { LoggerService } from '../services/logger/logger.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService,
                private router: Router,
                private logger: LoggerService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {

        return next.handle(req).pipe(
            catchError(this.handleError.bind(this))
        );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
          // A client-side or network error occurred. Handle it accordingly.
          this.logger.error('An error occurred:', error.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          this.logger.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
          // Handle Unauthorized
          if (error.status === 401) {
            // Comment for unauthorized testing purpose if you don't want your token to be destroyed
            this.authService.removeAuthorizationToken();
            this.router.navigate([{outlets: {modal: 'modal/login'}}], {state: {origin: this.router.url}});
          } else if (error.status == 422) {
            this.logger.error('An error occurred:', error.error);
          }
        }
        // return an observable with a user-facing error message
        return throwError(
          'Something bad happened; please try again later.');
      }
}
