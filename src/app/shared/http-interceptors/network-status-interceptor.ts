import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConnectionService } from '../services/connection/connection.service';
import { ToastService } from '../services/toast/toast.service';
import { throwError } from 'rxjs';

@Injectable()
export class NetworkStatusInterceptor implements HttpInterceptor {

    constructor(private connectionService: ConnectionService, private toastService: ToastService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        if (this.connectionService.isOnlineValue) {
            return next.handle(req);
        } else {
            this.toastService.showError('Network not available');
            return throwError('Network not available');
        }
    }
}
