import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionListComponent } from './components/question-list/question-list.component';
import { SkeletonModule } from 'src/app/shared/modules/skeleton/skeleton.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { QuestionFormComponent } from './components/question-form/question-form.component';
import { QuestionRoutingModule } from './question-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MathJaxModule } from 'ngx-mathjax';
import { KatexModule } from 'ng-katex';
import {MatRadioModule} from '@angular/material/radio';
import { SelectTypeModule } from '../select-type/select-type.module';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table'; 
import {MatCheckboxModule} from '@angular/material/checkbox';



@NgModule({
  declarations: [QuestionListComponent, QuestionFormComponent],
  imports: [
    CommonModule,
    QuestionRoutingModule,
    SkeletonModule,
    NgSelectModule,
    CKEditorModule,
    FormsModule,
    ReactiveFormsModule,
    KatexModule,
    MatRadioModule,
    SelectTypeModule,
    MatPaginatorModule,
    MatTableModule,
    MatCheckboxModule,
    // MathJaxModule.forRoot({
    //   version: '2.7.5',
    //   config: 'TeX-AMS_HTML',
    //   hostname: 'cdnjs.cloudflare.com'
    // })
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class QuestionModule { }
