import { Injectable } from '@angular/core';
import { NetworkService } from 'src/app/shared/services/network/network.service';
import { Api } from 'src/app/shared/constants/api';
import { HttpHeaders, HttpRequest, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { QuestionRequest } from 'src/app/shared/dtos/question/question-request';
import { Question } from 'src/app/shared/dtos/question/question';
import { PageRequest, Page } from 'src/app/shared/services/pagination/page';
import { QuestionQuery } from 'src/app/shared/dtos/question/question-query';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private networkService: NetworkService, private http: HttpClient) { }

  getPresignedUrlForUpload(name,node){
    return this.networkService.get<JSON>(Api.STORAGE_PRESIGNED_UPLOAD(name,node));
  }

  saveQuestion(json:String): Observable<[]>{
    return this.networkService.post(Api.SAVE_QUESTION,null,json);
  }

  updateQuestion(json:String): Observable<[]>{
    return this.networkService.put(Api.UPDATE_QUESTION,null,json);
  }

  fetchQuestion(request:QuestionRequest){
    return this.networkService.post<Question[]>(Api.SEARCH_QUESTIONS,null,request);
  }

  // fetchQuestion(request: PageRequest<QuestionResponse>, query: QuestionQuery): Observable<Page<QuestionResponse>> {
  //   // fake pagination, do your server request here instead
  //   return this.networkService.post<Page<QuestionResponse>>(Api.SEARCH_QUESTIONS,null,request);
  // }

  uploadfileAWSS3(fileuploadurl, contenttype, file): Observable<any>{ 
    //this will be used to upload all csv files to AWS S3
     const headers = new HttpHeaders({'Content-Type': contenttype});
     const req = new HttpRequest(
     'PUT',
     fileuploadurl,
     file,
     {
       headers: headers,
       reportProgress: true, //This is required for track upload process
     });
     return this.http.request(req);
    }

}
