import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SyllabusDetailType } from 'src/app/shared/enums/syllabus-detail-type';
import { MasterData } from 'src/app/shared/dtos/master-data';
import { QuestionRequest } from 'src/app/shared/dtos/question/question-request';
import { QuestionService } from '../../services/question.service';
import { Question } from 'src/app/shared/dtos/question/question';
import { PaginatedDataSource } from 'src/app/shared/services/pagination/paginated-datasource';
import { QuestionQuery } from 'src/app/shared/dtos/question/question-query';
import { PageRequest } from 'src/app/shared/services/pagination/page';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit {
  

  constructor(private elementRef: ElementRef,private router: Router, private route: ActivatedRoute, private questionService: QuestionService) { }

  type: SyllabusDetailType= SyllabusDetailType.QuestionForm;
  boardId;
  subjectId;
  standardId;
  selectedConcept:MasterData;
  displayedColumns = ['id', 'question', 'status', 'type']
  dataSource;
  // data = new PaginatedDataSource<QuestionResponse, QuestionQuery>(
  //   (request, query) => this.questionService.fetchQuestion(request, query),
  //   {property: 'id', order: 'desc'},
  //   {search: '',},
  //   2
  // )
  ngOnInit(): void {
    this.boardId= this.route.snapshot.paramMap.get('boardId');
    this.subjectId= this.route.snapshot.paramMap.get('subjectId');
    this.standardId= this.route.snapshot.paramMap.get('standardId');
    this.fetchQuestion();
  }

  ngOndestroy() {
    this.elementRef.nativeElement.remove();
  }

  navigateToQuestionForm(questionId) {
    var url = 'question/form';
    this.router.navigate([url]);
  }

  getMessage(concept: MasterData) {
    this.selectedConcept = concept;
  }

  fetchQuestion(){
    var concepts = []
    if(this.selectedConcept!=null){
      concepts.push(this.selectedConcept.id);
    }
    const request: QuestionRequest = {questionIds:null, conceptIds:concepts,size:10,start:0};
   // const request: PageRequest<QuestionResponse> = {conceptIds:[1],size:10,page:0};
    this.questionService.fetchQuestion(request).subscribe((data)=>{
      this.dataSource = new MatTableDataSource<Question>(data);
    })

  }

  editQuestionForm(data){
    var url = 'question/form/'+data.id;
    this.router.navigate([url]);
  }

}
