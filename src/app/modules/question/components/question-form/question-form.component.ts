import { Component, OnInit, ElementRef } from '@angular/core';

import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import SimpleUploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';
import { UploadAdapter } from 'src/app/shared/services/ckeditor/upload-adaptor';
import { CloudinaryImageUploadAdapter } from 'ckeditor-cloudinary-uploader-adapter';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionService } from '../../services/question.service';
import { combineLatest, Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { masterSelectorBoardsDictionary, masterSelectorBTDictionary, masterSelectorBT, masterSelectorDifficultyLevel, masterSelectorQuestionType, masterSelectorQuestionStatus } from 'src/app/shared/modules/store/master/master.selector';
import { first } from 'rxjs/operators';
import { MasterData } from 'src/app/shared/dtos/master-data';
import { AppState } from 'src/app/shared/modules/store';
import { loadBT, loadDifficultyLevel, loadQuestionType, loadQuestionStatus } from 'src/app/shared/modules/store/master/master.action';
import { SyllabusDetailType } from 'src/app/shared/enums/syllabus-detail-type';
import { QuestionRequest } from 'src/app/shared/dtos/question/question-request';
import { Choice } from 'src/app/shared/dtos/question/choice';
import { Api } from 'src/app/shared/constants/api';

@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.scss']
})
export class QuestionFormComponent implements OnInit {

  type: SyllabusDetailType= SyllabusDetailType.QuestionForm;
  board:any;
  boardId;
  subjectId;
  standardId;
  questionId;
  ckeConfig: any;
  questionForm: FormGroup;
  public Editor = ClassicEditor;
  httpClient: HttpClient;
  bts: MasterData[];
  difficultyLevels: MasterData[];
  questionTypes:MasterData[];
  questionStatuses:MasterData[];
  isUpdate: boolean=false;
  selectedConcept: MasterData;
  conceptObservable: Observable<MasterData>;

  fileToUpload: File;
  html: string = `
  You can write text, that contains expressions like this: $x ^ 2 + 5$ inside them. As you probably know. You also can write expressions in display mode as follows: $$\sum_{i=1}^n(x_i^2 - \overline{x}^2)$$. In first case you will need to use \$expression\$ and in the second one \$\$expression\$\$. To scape the \$ symbol it's mandatory to write as follows: \\$
  `;

  editorConfig = {
    placeholder: 'Type the content here!',
    extraPlugins: [ this.imagePluginFactory ],
  };

  onReady(eventData) {
    eventData.plugins.get('FileRepository').createUploadAdapter = function (loader) {
      console.log(btoa(loader.file));
      return new UploadAdapter(loader,'',this.httpClient);
    };
  }
  
  imagePluginFactory(editor) {
    editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
      return new CloudinaryImageUploadAdapter( 
        loader,
        'your_cloud_name',
        'your_unsigned_upload_preset',
        [ 160, 500, 1000, 1052 ]
      );
    };
  }

  constructor(private elementRef: ElementRef,private router: Router,private store: Store<AppState>,private questionService: QuestionService,private fb:FormBuilder,private route: ActivatedRoute) { }

  ngOndestroy() {
    this.elementRef.nativeElement.remove();
  }

  ngOnInit() {

    // this.conceptObservable = Observable.create(observer => {
    //   const interval = setInterval(() => {
    //     observer.next(this.selectedConcept)
    //   }, 1)

    //   return () => clearInterval(interval)
    // });

    this.boardId= this.route.snapshot.paramMap.get('boardId');
    this.subjectId= this.route.snapshot.paramMap.get('subjectId');
    this.standardId= this.route.snapshot.paramMap.get('standardId');
    this.questionId= this.route.snapshot.paramMap.get('questionId');

    this.initializeForm();
    this.loadData();
    if(this.questionId!=null){
      this.isUpdate=true;
      this.populateForm();
    }
  
    
    
    this.ckeConfig = {
      toolbar: ['imageUpload',  'heading', '|', 'bold', 'italic' ],
                    ckfinder: {
                      options: {
                        resourceType: 'Images'
                      },
                      uploadUrl: Api.BASE_URL+Api.UPLOAD_FILE
                     }
      
    };
  }

  initializeForm(){
    this.questionForm = this.fb.group({
      id:[''],
      question: [''],
      questionStatusId:[''],
      solution: [''],
      bloomsTaxonomyId:[''],
      difficultyLevelId:[''],
      questionTypeId:[''],
      concepts:[''],
        choices: this.fb.array([
          //this.initProduct()
        ])
    });
    this.setChoices();
  }

  populateForm(){
    var questionIds = [];
    questionIds.push(this.questionId)
    const request: QuestionRequest = {questionIds:questionIds, conceptIds:null,size:null,start:null};
    this.questionService.fetchQuestion(request).subscribe((data)=>{
      if(data!=null && data.length!=0){
        var q = data[0];
        this.questionForm = this.fb.group({
          id:[q.id],
          question: [q.question],
          questionStatusId:[q.questionStatus.id],
          solution: [q.solution],
          bloomsTaxonomyId:[q.bloomsTaxonomy.id],
          difficultyLevelId:[q.difficultyLevel.id],
          questionTypeId:[q.questionType.id],
          concepts:q.concepts,
            choices: 
              this.populuateChoices(q.choices)
        });
        //this.conceptObservable.subscribe((value) => this.selectedConcept = q?.concepts[0]);
        this.selectedConcept  = q?.concepts[0];
        //this.setChoicesData(q.choices);
      }
    })
  }
  populuateChoices(choices){
    const data: FormArray = this.fb.array([]);
    choices.forEach(x => {
      data.push(this.populateChoiceData(x));
    })
    return data;
  }

  initChoice(){
    return this.fb.group({
      choice: [''],
    })
  }

  getMessage(concept: MasterData) {
    if(concept!=null && concept!= undefined){
    this.selectedConcept = concept;
    }
  }

  getPresignedUrlAndUpload(){
    this.questionService.getPresignedUrlForUpload(name,"name").subscribe((data)=>{
      if(data['status']=='success'){
        this.uploadToS3(data['data']['url']['url']);
      }
    })
  }

  uploadToS3(url){
    console.log("file type: "+this.fileToUpload.type);
    this.questionService.uploadfileAWSS3(url, 'application/octet-stream',this.fileToUpload).subscribe((event: HttpEvent<any>) => {
      });
  }

  setChoices() {
    let control = <FormArray>this.questionForm.controls.choices;
    for (let index = 0; index < 4; index++) {
      control.push(this.populateText());
    }
  }

  populateChoiceData(choice: Choice){
    return this.fb.group({
      id:choice.id,
      choice: choice.choice,
      isCorrect: choice.isCorrect

    })
  }

  populateText(){
    return this.fb.group({
      id:'',
      choice: '',
      isCorrect:''
    })
  }

  addChoice(){
    const aFormGroup = this.questionForm as FormGroup;
    const aFormArray = aFormGroup.controls.choices as FormArray;
    aFormArray.push(this.initChoice());
  }

  removeChoice(i: number){
    const aFormGroup = this.questionForm as FormGroup;
    const aFormArray = aFormGroup.controls.choices as FormArray;
    aFormArray.removeAt(i); 
  }

  getQuestionFormControls(){
    let control = <FormArray>this.questionForm?.controls?.choices;
    return control.controls;
  }

  getQuestionValueControl(){
    let control = <FormArray>this.questionForm.controls.question;
    return control.controls;
  }

  loadData(){
    this.store.dispatch(loadBT());
    this.store.dispatch(loadDifficultyLevel());
    this.store.dispatch(loadQuestionType());
    this.store.dispatch(loadQuestionStatus());
    // this.store.dispatch(loadStandards());
    // this.store.dispatch(loadSubjects());
    this.fetchMasterData();
  }

  fetchMasterData(){
    combineLatest([
      this.store.pipe(select(masterSelectorBT)),
      this.store.pipe(select(masterSelectorDifficultyLevel)),
      this.store.pipe(select(masterSelectorQuestionType)),
      this.store.pipe(select(masterSelectorQuestionStatus))
      // this.store.pipe(select(masterSelectorBoardsDictionary)),
      // this.store.pipe(select(masterSelectorStandardsDictionary)),
    ]).pipe(first(([bts,dl,type,status]) => {
      return Object.keys(bts).length > 0 && Object.keys(dl).length > 0 && Object.keys(type).length > 0 && Object.keys(status).length > 0;
    })).subscribe(([bts,dl,type,status]) => {
      this.bts = bts;
      this.difficultyLevels = dl;
      this.questionTypes=type;
      this.questionStatuses = status;
      //this.subjects = this.getArrayFromDictionary(boards['subjects']);
      //this.standards = this.getArrayFromDictionary(standards);
    });
  }

  save(model) {
    if(this.questionForm.valid){
      var concepts : MasterData[]=[];
      concepts.push(this.selectedConcept)
      this.questionForm.get('concepts').patchValue(concepts);

    // call API to save customer
    console.log(JSON.stringify(model.value));
    if(this.isUpdate){
      // this.questionService.updateSyllabusNode(JSON.stringify(model.value),this.node.id).subscribe((data)=>{
      //   if(this.fileToUpload!=null){
      //     this.uploadFileToActivity();
      //   }
      // });
      this.questionService.updateQuestion(JSON.stringify(model.value)).subscribe((data)=>{
        this.successDial("Sucessfully Updated");
      });
    } else{
      this.questionService.saveQuestion(JSON.stringify(model.value)).subscribe((data)=>{
        this.successDial("Sucessfully Saved");
      });
      }
    }
  }

  private successDial(message: String){
    window.alert(message);
    //location.reload();
    this.router.navigate(['question/list']);

  }
  

}
