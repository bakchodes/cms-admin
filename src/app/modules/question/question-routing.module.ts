import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionListComponent } from './components/question-list/question-list.component';
import { QuestionFormComponent } from './components/question-form/question-form.component';


const routes: Routes = [
  {
    path: 'list',
    component: QuestionListComponent
  },
  {
    path: 'list/:boardId/:standardId/:subjectId',
    component: QuestionListComponent
  },
  {
    path: 'form/:boardId/:standardId/:subjectId/:questionId',
    component: QuestionFormComponent
  },
  {
    path: 'form/:questionId',
    component: QuestionFormComponent
  },
  {
    path: 'form',
    component: QuestionFormComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionRoutingModule { }
