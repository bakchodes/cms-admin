import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-content-type',
  templateUrl: './content-type.component.html',
  styleUrls: ['./content-type.component.scss']
})
export class ContentTypeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  navigateToSyllabus() {
    this.router.navigate(['select/syllabus']);
  }

}
