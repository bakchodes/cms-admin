import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SyllabusDetailFormComponent } from './syllabus-detail-form.component';

describe('SyllabusDetailFormComponent', () => {
  let component: SyllabusDetailFormComponent;
  let fixture: ComponentFixture<SyllabusDetailFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyllabusDetailFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SyllabusDetailFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
