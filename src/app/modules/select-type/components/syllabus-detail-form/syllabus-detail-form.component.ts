import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { loadBoards } from 'src/app/shared/modules/store/master/master.action';
import { Router } from '@angular/router';
import { AppState } from 'src/app/shared/modules/store';
import { Store, select } from '@ngrx/store';
import { NgSelectComponent } from '@ng-select/ng-select';
import { masterSelectorBoards } from 'src/app/shared/modules/store/master/master.selector';
import { combineLatest } from 'rxjs';
import { first } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { SyllabusDetailType } from 'src/app/shared/enums/syllabus-detail-type';
import { MasterData } from 'src/app/shared/dtos/master-data';

@Component({
  selector: 'app-syllabus-detail-form',
  templateUrl: './syllabus-detail-form.component.html',
  styleUrls: ['./syllabus-detail-form.component.scss']
})
export class SyllabusDetailFormComponent implements OnInit {

  @Input() type: SyllabusDetailType;
  @Input() concept: MasterData;

  @Output() selectedConceptEmit = new EventEmitter<MasterData>();

  disableSelect = new FormControl(false);

  constructor(private router: Router, private store: Store<AppState>) { }

  ngOnInit() {
    this.LoadData();
  }

  SyllabusType = SyllabusDetailType;
  selectedBoard=null;
  selectedStandard = null;
  selectedSubject = null;
  selectedChapter = null;
  selectedConcept=null;
  boards = null;
  standards = null;
  subjects = null;
  chapters = null;
  concepts = null;

  @ViewChild('standardC') standardComponent: NgSelectComponent;
  @ViewChild('subjectC') subjectComponent: NgSelectComponent;
  @ViewChild('boardC') boardComponent: NgSelectComponent;
  @ViewChild('chapterC') chapterComponent: NgSelectComponent;
  @ViewChild('conceptC') conceptComponent: NgSelectComponent;

  navigateToQuestions() {
    this.router.navigate(['question/list']);
  }

  LoadData() {
    this.store.dispatch(loadBoards());
    // this.store.dispatch(loadStandards());
    // this.store.dispatch(loadSubjects());
    this.fetchMasterData();
  }
  fetchMasterData() {
    combineLatest([
      this.store.pipe(select(masterSelectorBoards)),
      // this.store.pipe(select(masterSelectorBoardsDictionary)),
      // this.store.pipe(select(masterSelectorStandardsDictionary)),
    ]).pipe(first(([boards]) => {
      return Object.keys(boards).length > 0;
    })).subscribe(([boards]) => {
      this.boards = boards;
      if(this.concept!=null){
        this.populateData();
      }
      //this.subjects = this.getArrayFromDictionary(boards['subjects']);
      //this.standards = this.getArrayFromDictionary(standards);
    });
  }

  populateData(){
    this.boards.forEach(b => {
      var standards = b['standards'];
      standards.forEach(s => {
        var subjects = s['subjects'];
        subjects.forEach(sub => {
          var chapters = sub['chapters']
          chapters.forEach(chap => {
            var concepts = chap['concepts'];
            concepts.forEach(c => {
              if(c['id']==this.concept.id){
                this.selectedConcept=this.concept;
                this.selectedChapter=chap;
                this.selectedSubject=sub;
                this.selectedStandard=s;
                this.selectedBoard=b;
                return;
              }
            });
          });
        });
      });
    });
  }

  getArrayFromDictionary(dictionary) {
    return Object.keys(dictionary).map(function (key) {
      return dictionary[key];
    });
  }

  boardSelected($event) {
    this.clearStandard();
    this.clearSubject();
    if(this.type==SyllabusDetailType.QuestionForm){
      this.clearChapter();
      this.clearConcept();
    }
    this.selectedBoard = $event;
    this.standards = $event['standards'];

  }

  standardSelected($event) {
    this.clearSubject();
    if(this.type==SyllabusDetailType.QuestionForm){
      this.clearChapter();
      this.clearConcept();
    }
    this.selectedStandard = $event;
    this.subjects = $event['subjects'];
  }
  subjectSelected($event){
    if(this.type==SyllabusDetailType.QuestionForm){
      this.clearChapter();
      this.clearConcept();
    }
    this.selectedSubject = $event;
    this.chapters = $event['chapters'];
  }

  chapterSelected($event){
    if(this.type==SyllabusDetailType.QuestionForm){
      this.clearConcept();
    this.selectedChapter = $event;
    this.concepts = $event['concepts'];
    }
  }

  clearStandard(){
    this.selectedStandard = null;
    this.standards = null;
    this.standardComponent.handleClearClick();
  }
  clearSubject(){
    this.selectedSubject=null;
    this.subjects=null;
    this.subjectComponent.handleClearClick();
  }
  clearChapter(){
    this.selectedChapter=null;
    this.chapters=null;
    this.chapterComponent.handleClearClick();

  }
  clearConcept(){
    this.concepts=null;
    this.selectedConcept=null;
    this.conceptComponent.handleClearClick();
  }

  sendMessageToParent($event) {
    this.selectedConceptEmit.emit($event);
  }

  submit($event) {
    this.selectedSubject = $event;
    console.log(this.selectedBoard);
    console.log(this.selectedStandard);
    console.log(this.selectedSubject);
    console.log(this.selectedConcept);
    this.router.navigate(['question/list/' + this.selectedBoard['id'] + '/' + this.selectedStandard['id'] + '/' + this.selectedSubject['id']]);
  }


}
