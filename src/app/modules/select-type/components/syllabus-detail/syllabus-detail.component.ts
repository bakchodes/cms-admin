import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MasterDataService } from 'src/app/shared/services/master-data/master-data.service';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/shared/modules/store';
import { loadBoards, loadStandards, loadSubjects } from 'src/app/shared/modules/store/master/master.action';
import { masterSelectorBoardsDictionary, masterSelectorStandardsDictionary, masterSelectorSubjectsDictionary, masterSelectorBoards } from 'src/app/shared/modules/store/master/master.selector';
import { combineLatest } from 'rxjs';
import { first } from 'rxjs/operators';
import { NgSelectComponent } from '@ng-select/ng-select';
import { SyllabusDetailType } from 'src/app/shared/enums/syllabus-detail-type';

@Component({
  selector: 'app-syllabus-detail',
  templateUrl: './syllabus-detail.component.html',
  styleUrls: ['./syllabus-detail.component.scss']
})
export class SyllabusDetailComponent implements OnInit {

  constructor(private router: Router, private store: Store<AppState> ) { }

  ngOnInit() {
    this.LoadData();
  }

  type: SyllabusDetailType = SyllabusDetailType.SyllabusDetail;

  selectedBoard = {};
  selectedStandard = {};
  selectedSubject = {};
  boards = [];
  standards = [];
  subjects = [];

  @ViewChild('standardC') standardComponent: NgSelectComponent; 
  @ViewChild('subjectC') subjectComponent: NgSelectComponent; 
  @ViewChild('boardC') boardComponent: NgSelectComponent; 

navigateToQuestions() {
  this.router.navigate(['question/list']);
}

LoadData(){
  this.store.dispatch(loadBoards());
  // this.store.dispatch(loadStandards());
  // this.store.dispatch(loadSubjects());
  this.fetchMasterData();
}
fetchMasterData(){
  combineLatest([
    this.store.pipe(select(masterSelectorBoards)),
    // this.store.pipe(select(masterSelectorBoardsDictionary)),
    // this.store.pipe(select(masterSelectorStandardsDictionary)),
  ]).pipe(first(([boards]) => {
    return Object.keys(boards).length > 0;
  })).subscribe(([boards]) => {
    this.boards = boards;
    //this.subjects = this.getArrayFromDictionary(boards['subjects']);
    //this.standards = this.getArrayFromDictionary(standards);
  });
}

getArrayFromDictionary(dictionary){
  return Object.keys(dictionary).map(function(key){
    return dictionary[key];
});
}

boardSelected($event){
  this.selectedStandard={};
  this.standards=[];
  this.subjects=[];
  this.selectedSubject={};
  this.standardComponent.handleClearClick();
  this.subjectComponent.handleClearClick();
  this.selectedBoard = $event;
  this.standards = $event['standards'];

}

standardSelected($event){
  this.subjects=[];
  this.selectedSubject={};
  this.subjectComponent.handleClearClick();
  this.selectedStandard = $event;
  this.subjects = $event['subjects'];
}
submit($event){
  this.selectedSubject = $event;
  console.log(this.selectedBoard);
  console.log(this.selectedStandard);
    console.log(this.selectedSubject);
    this.router.navigate(['question/list/'+this.selectedBoard['id']+'/'+this.selectedStandard['id']+'/'+this.selectedSubject['id']]);
}



}
