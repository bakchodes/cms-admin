import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentTypeComponent } from './components/content-type/content-type.component';
import { SyllabusDetailComponent } from './components/syllabus-detail/syllabus-detail.component';
import { SyllabusDetailFormComponent } from './components/syllabus-detail-form/syllabus-detail-form.component';


const routes: Routes = [
  {
    path: 'type',
    component: ContentTypeComponent
  },
  {
    path: '',
    component: ContentTypeComponent
  },
  {
    path: 'syllabus',
    component: SyllabusDetailComponent
  },
  {
    path: 'demo',
    component: SyllabusDetailFormComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SelectTypeRoutingModule { }
