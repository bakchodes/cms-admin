import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentTypeComponent } from './components/content-type/content-type.component';
import { SelectTypeRoutingModule } from './select-type-routing.module';
import { NgbRating, NgbAccordion } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SkeletonModule } from 'src/app/shared/modules/skeleton/skeleton.module';
import { SyllabusDetailComponent } from './components/syllabus-detail/syllabus-detail.component';
import { SyllabusDetailFormComponent } from './components/syllabus-detail-form/syllabus-detail-form.component';
import {MatSelectModule} from '@angular/material/select';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';



@NgModule({
  declarations: [ContentTypeComponent, SyllabusDetailComponent, SyllabusDetailFormComponent],
  imports: [
    CommonModule,
    SelectTypeRoutingModule,
    SkeletonModule,
    NgSelectModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [SyllabusDetailFormComponent,ContentTypeComponent,SyllabusDetailComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SelectTypeModule { }
