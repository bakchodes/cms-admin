import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-modalview',
  templateUrl: './modalview.component.html',
  styleUrls: ['./modalview.component.scss'],
  // host:{"(click)": "handleClick( $event )"},
})
export class ModalViewComponent {

  private elementRef: ElementRef;
  private router: Router;

  constructor(
    elementRef: ElementRef,
    router: Router,
    private location: Location
  ) {

    this.elementRef = elementRef;
    this.router = router;

  }

  // ---
  // PUBLIC METHODS.
  // ---

  public closeCurrentModal(): void {
    this.location.back();
  }

  public closeAllModals(): void {
    this.router.navigate([{ outlets: { modal: null } }], {replaceUrl: true});
  }


  // I handle a click on the modal-view.
  @HostListener('click', ['$event'])
  public handleClick(event: MouseEvent): void {

    // If the user clicked directly on the modal backdrop, let's treat that as a
    // desire to close the modal window - empty the auxiliary route.
    if (event.target === this.elementRef.nativeElement) {

      // this.closeModal();

    }

  }

}
