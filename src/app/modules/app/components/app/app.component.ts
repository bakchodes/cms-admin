import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from '../../../../shared/services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService, private title: Title,
    private meta: Meta) {
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      // this.router.navigate(['create-profile']);
    }

    this.title.setTitle('TestKaro');
    this.meta.updateTag({ name: 'description', content: 'TestKaro' });
  }
}

