// toast.component.ts
import {Component, TemplateRef, HostBinding} from '@angular/core';
import {ToastService} from '../../../../shared/services/toast/toast.service';

@Component({
  selector: 'app-toasts',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss']
})
export class ToastComponent {

  @HostBinding('class.ngb-toasts') toastClass = 'true';
  constructor(public toastService: ToastService) {}

  isTemplate(toast) { return toast.textOrTpl instanceof TemplateRef; }
}
