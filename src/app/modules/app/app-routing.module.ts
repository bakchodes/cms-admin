import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/shared/services/auth-guard/auth-guard.service';
import { ModalViewComponent } from './components/modalview/modalview.component';
import { NonAuthGuardService } from 'src/app/shared/services/non-auth-guard/non-auth-guard.service';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    loadChildren: () => import('../select-type/select-type.module').then(mod => mod.SelectTypeModule)
  },
  {
    path: 'register',
    canActivate: [NonAuthGuardService],
    loadChildren: () => import('../common/register-page/register-page.module').then(mod => mod.TRegisterModule)
  },
  {
    path: 'login',
    canActivate: [NonAuthGuardService],
    loadChildren: () => import('../common/login-page/login-page.module').then(mod => mod.LoginModule)
  },
  {
    path: 'select',
    loadChildren: () => import('../select-type/select-type.module').then(mod => mod.SelectTypeModule)
  },
  {
    path: 'question',
    loadChildren: () => import('../question/question.module').then(mod => mod.QuestionModule)
  },
  {
    path: 'modal',
    outlet: 'modal',
    component: ModalViewComponent,
    children: [
      {
        path: 'verify-otp',
        canActivate: [NonAuthGuardService],
        loadChildren: () => import('../../shared/modules/v-otp/v-otp.module').then(mod => mod.VOtpModule)
      },
      {
        path: 'register',
        canActivate: [NonAuthGuardService],
        loadChildren: () => import('../../shared/modules/register-modal/register-modal.module').then(mod => mod.RegisterModalModule)
      },
      {
        path: 'login',
        canActivate: [NonAuthGuardService],
        loadChildren: () => import('../../shared/modules/login-modal/login-modal.module').then(mod => mod.LoginModalModule)
      },
    ]
  },
  {
    path: '**',
    redirectTo: ''
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload',
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
