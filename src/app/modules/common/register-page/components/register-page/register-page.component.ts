import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  submitted = false;
  state: { parent: string };

  constructor(
    public activatedRoute: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    this.setState();
  }

  setState(): void {
    this.state = window.history.state;
  }

  navigateToLogin(): void {
    this.router.navigate(['/login']);
  }

}
