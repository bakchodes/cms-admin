import { NgModule } from '@angular/core';
import { CommonModule, TitleCasePipe } from '@angular/common';

import { SkeletonModule } from '../../../shared/modules/skeleton/skeleton.module';
import { RegisterFormModule } from 'src/app/shared/modules/register-form/register-form.module';
import { RegisterPageComponent } from './components/register-page/register-page.component';
import { RegisterPageRoutingModule } from './register-page-routing.module';

@NgModule({
  declarations: [RegisterPageComponent],
  imports: [
    CommonModule,
    SkeletonModule,
    RegisterFormModule,
    RegisterPageRoutingModule,
  ],
})
export class TRegisterModule { }
